﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace AndeSAT
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void btnPassenger_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/GUI/MapPage.xaml", UriKind.Relative));
        }

        private void btnDriver_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/GUI/Driver.xaml", UriKind.Relative));
        }
    }
}