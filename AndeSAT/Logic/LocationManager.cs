﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Device.Location;


namespace AndeSAT.Logic
{    
    /**
     * Revisa la localización del usuario mientras la red esté disponible.
     */
    public class LocationManager
    {

        //-----------------------------------------------------------------------------
        //ATRIBUTOS
        //-----------------------------------------------------------------------------
        /**
         * Revisa los cambios de localización del dispositivo.
         */
        private GeoCoordinateWatcher watcher;

        /**
         * Almacena las coordenadas de la posición que recibe el "watcher" cuando cambia de estado.
         */
        private GeoCoordinate epl;


        /**
         * Instancia del objeto para ser retornado en caso de ya haberse inicializado alguna vez en el ciclo de ejecución.
         */
        private static LocationManager instancia;


        //-----------------------------------------------------------------------------
        //CONSTRUCTORES
        //-----------------------------------------------------------------------------
        /**
         * iNICIALIZA EL LOCALIZADOR.
         */
        public LocationManager()
        {
            watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default)
            {
                MovementThreshold = 20
            };

            watcher.PositionChanged += this.watcher_PositionChanged;
            watcher.StatusChanged += this.watcher_StatusChanged;
            watcher.Start();
        }

        /**
         * Retorna una instancia de la clase si ya está inicializada, de lo contrario inicializa la intancia y la retorna.
         */
        public static LocationManager darInstancia()
        {
            if (instancia == null)
            {
                instancia = new LocationManager();
            }
            return instancia;

        }


        //-----------------------------------------------------------------------------
        //MÉTODOS
        //-----------------------------------------------------------------------------
       /**
        * Evento que se lanza cuando el "watcher" cambia de estado.
        */
        private void watcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            if (watcher.Permission == GeoPositionPermission.Denied)
            {
                MessageBox.Show("No hay permisos para acceder a los servicios de localización.");
            }
            switch (e.Status)
            {

                case GeoPositionStatus.Disabled:
                    // location is unsupported on this device
                    MessageBox.Show("Los servicios de localización están deshabilitados en el dispositivo.");
                    break;
                case GeoPositionStatus.NoData:
                    // data unavailable
                    MessageBox.Show("Los servicios de localización están habilitados pero no pueden mostrar la localización");
                    break;
            }
        }

        /**
         * Evento que se lanza cuando el "watcher" detecta un cambio de posición.
         */
        private void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            epl = e.Position.Location;
            if (epl.IsUnknown)
            {
                MessageBox.Show("Please wait while your prosition is determined....");
                return;
            }          

        }

        /*
         * Retorna las coordenadas de la localización actual.
         */
        public GeoCoordinate darLocalizacion()
        {
            watcher.TryStart(true, new TimeSpan(0,0,1));
            return epl;
        }

    }
}
