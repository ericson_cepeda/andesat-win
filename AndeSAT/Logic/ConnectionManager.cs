﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace AndeSAT.Logic
{
    /**
     * Administra la conexión al web service para enviar y recibir los datos de la aplicación.
     */
    public class ConnectionManager
    {

        const String UPLOAD_ADDRESS = "";

        const String DOWNLOAD_ADDRESS = "";


        //-----------------------------------------------------------------------------
        //ATRIBUTOS
        //-----------------------------------------------------------------------------
        /**
         * La respuesta obtenida del servicio.
         */
        public String resultado { get; set; }

        /**
         * Indica si ya se obtuvo la respuesta del servicio web.
         */
        public bool listo { get; set; }

        private WebClient client;

        private static ConnectionManager instance;


        //-----------------------------------------------------------------------------
        //CONSTRUCTORES
        //-----------------------------------------------------------------------------
        /**
         * Inicializa el administrador.
         */
        public ConnectionManager()
        {
            listo = false;
            client = new WebClient();
            //scs = new SCService.SCServiceClient();
        }

        public static ConnectionManager getInstance()
        {
            if (instance == null)
            {
                instance = new ConnectionManager();
            }
            return instance;
        }

        //-----------------------------------------------------------------------------
        //MÉTODOS
        //-----------------------------------------------------------------------------
        /**
         * Consulta los incidentes registrados en el servidor web.
         */
        public void consultarIncidentes()
        {             
            //scs.darIncidentesAsync();
            //scs.darIncidentesCompleted += new EventHandler<SCService.darIncidentesCompletedEventArgs>(scs_darIncidentesCompleted);
        }

        public void sendLocation(User user)
        {
            String json = JsonConvert.SerializeObject(user);
            Uri uri = new Uri(UPLOAD_ADDRESS);
            client.UploadStringAsync(uri, json);
        }



        
        /**
         * Evento que se lanza cuando se completa el método de pedir los incidentes.
         */
        /*
        private void scs_darIncidentesCompleted(object sender, SCService.darIncidentesCompletedEventArgs e)
        {
            resultado = e.Result.ToString();
            listo = true;
            MessageBox.Show(resultado);
        }
        */

        /**
         * Realiza la petición al servidor de registrar el incidente dado por parámetro.
         */
        /*
        public void registrarIncidente(Incidente inc)
        {
            scs.registrarIncidenteAsync(inc.darTipoInt(inc.tipo), inc.latitud, inc.longitud, inc.descripcion, inc.fechaOcurrencia.ToShortDateString());
            
            scs.registrarIncidenteCompleted += new EventHandler<SCService.registrarIncidenteCompletedEventArgs>(scs_registrarIncidenteCompleted);
        }
        */

        /**
         * Evento que se lanza cuando se completa el método de registrar un incidente.
         */
        /*
        private void scs_registrarIncidenteCompleted(object sender, SCService.registrarIncidenteCompletedEventArgs e)
        {
            resultado = e.Result.ToString();
            MessageBox.Show(resultado);
        }
        */
    }
}
