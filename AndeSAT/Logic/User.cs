﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AndeSAT.Logic
{
    public class User
    {
        /**
    * The name of the user.
    */
        protected String name;

        /**
         * The phone number of the user.
         */
        protected String phoneNumber;

        /**
         * The e-mail address of the user.
         */
        protected String email;

        /**
         * Current latitude coordinate of the user.
         */
        protected double latitude;

        /**
         * Current longitude coordinate of the user.
         */
        protected double longitude;


        /**
         * Initilizes the cobject.
         */
        public User()
        {

        }



        /**
         * Gives the name of the user.
         * @return name
         */
        public String getName()
        {
            return name;
        }

        /**
         * Changes the name of the user for the parameter
         * @param name the new name to set.
         */
        public void setName(String name)
        {
            this.name = name;
        }

        /**
         * Returns the number of the user's phone.
         * @return phoneNumber
         */
        public String getPhoneNumber()
        {
            return phoneNumber;
        }

        /**
         * Changes the number of the user for the parameter.
         * @param phoneNumber The new number.
         */
        public void setPhoneNumber(String phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }

        /**
         * Gets the e-mail address of the user.
         * @return
         */
        public String getEmail()
        {
            return email;
        }

        /**
         * Sets the user e-mail address with the parameter
         * @param email
         */
        public void setEmail(String email)
        {
            this.email = email;
        }


        /**
         * Gets the latitude of the user.
         * @return
         */
        public double getLatitude()
        {
            return latitude;
        }

        /**
         * Sets the user latitude with the parameter
         * @param latitude
         */
        public void setLatitude(double latitude)
        {
            this.latitude = latitude;
        }

        /**
         * Gets the longitude of the user.
         * @return
         */
        public double getLongitude()
        {
            return longitude;
        }

        /**
         * Sets the user longitude with the parameter
         * @param longitude
         */
        public void setLongitude(double longitude)
        {
            this.longitude = longitude;
        }



    }
}
