﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Device.Location;
using System.Collections.Generic;
using Microsoft.Phone.Controls.Maps;

namespace AndeSAT.Logic
{
    

    public class Main
    {
        private LocationManager location;

        private ConnectionManager connection;

        private static Main instance;

        private User user;

        public Main()
        {
            location = LocationManager.darInstancia();
            user = new User();
        }

        public static Main getInstance()
        {
            if (instance == null)
            {
                instance = new Main();
            }
            return instance;
        }

        public GeoCoordinate getLocation()
        {
            return location.darLocalizacion();
        }

        public Stack<Pushpin> getOthersPushpins()
        {
            Stack<Pushpin> pushpins = new Stack<Pushpin>();

            return pushpins;
        }

        public void sendLocation()
        {
            location = LocationManager.darInstancia();
            GeoCoordinate coordinate = location.darLocalizacion();
            double latitude = coordinate.Latitude;
            double longitude = coordinate.Longitude;
            user.setLatitude(latitude);
            user.setLongitude(longitude);
            connection = ConnectionManager.getInstance();
            connection.sendLocation(user);
        }

    }
}
