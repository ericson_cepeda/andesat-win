﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Maps;
using System.Device.Location;
using AndeSAT.Logic;

namespace AndeSAT.GUI
{
    public partial class MapPage : PhoneApplicationPage
    {
        private Main main;

        private Stack<Pushpin> pushpins;

        public MapPage()
        {
            InitializeComponent();
            pushpins = new Stack<Pushpin>();
            main = Main.getInstance();
            showLocation();
        }

        private void buttonRoad_Click(object sender, RoutedEventArgs e)
        {
            map1.Mode = new RoadMode();
        }

        private void buttonAerial_Click(object sender, RoutedEventArgs e)
        {
            map1.Mode = new AerialMode();
        }

        private void showLocation()
        {
            GeoCoordinate epl = main.getLocation();
            Pushpin pin1 = new Pushpin();
            pin1.Location = epl;
            map1.Center = epl;
            map1.Children.Add(pin1);

        }
    }
}